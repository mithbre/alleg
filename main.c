// gcc main.c `allegro-config --libs` -o main.o
#include <allegro.h>
#define TILE 32
#define DWN  1
#define LFT  2
#define UP   3
#define RT   0

int main()
{
	allegro_init();
	install_keyboard();
	set_color_depth(16);
	set_gfx_mode(GFX_AUTODETECT_WINDOWED, 640,480,0,0);

	char dir = 1, direction = RT;
	int sprY = TILE * 14;
	int sprX = 0;
	// Load images
	BITMAP *bg = NULL;
	BITMAP *spr = NULL;
	bg = load_bitmap("bg.bmp", NULL);
	spr = load_bitmap("32x32.bmp", NULL);
	clear_bitmap(screen);

	BITMAP *screen_buff = NULL;
	screen_buff = create_bitmap(TILE * 20, TILE * 15);

	while (!key[KEY_ESC]) {
		if (keypressed()) {
			switch (readkey() & 0xff) {
			case 'w':
				direction = UP;
				sprY -= TILE;
				break;
			case 'a':
				direction = LFT;
				sprX -= TILE;
				break;
			case 's':
				direction = DWN;
				sprY += TILE;
				break;
			case 'd':
				direction = RT;
				sprX += TILE;
				break;
			}
		} clear_keybuf();

		// display images on screen
		blit(bg, screen_buff, 0,0,0,0,640,480);
		rotate_sprite(screen_buff, spr, 0, 0, itofix(dir++));

		switch (direction) {
		case RT:
			draw_sprite(screen_buff, spr, sprX, sprY); break;
		case DWN:
			rotate_sprite(screen_buff, spr, sprX, sprY, itofix(64)); break;
		case LFT:
			draw_sprite_h_flip(screen_buff, spr, sprX, sprY); break;
		case UP:
			rotate_sprite(screen_buff, spr, sprX, sprY, itofix(64 * 3)); break;
		}
		blit(screen_buff, screen, 0,0,0,0,640,480);
		rest(5);
	}

	destroy_bitmap(bg);
	destroy_bitmap(spr);
	return 0;
}
